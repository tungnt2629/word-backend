<?php

namespace App\Http\Controllers;

use App\Events\DocumentCreatedEvent;
use App\Models\Document;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DocumentController extends Controller
{
    public function list(Request $request)
    {
        $query = Document::query();
        $data = $request->all();

        if (isset($data['keyword'])) {
            $query->where(function ($q) use ($data) { 
                $q->where('title', 'like', '%'.$data['keyword'].'%');
            });
        }

        $query = $query->with('department');

        $list = $query->paginate(100);

        $list = json_encode($list);
        $list = json_decode($list);

        $response = [
            'pagination' => [
                'total' => $list->total,
                'perPage' => $list->per_page,
                'currentPage' => $list->current_page,
                'lastPage' => $list->last_page,
                'from' => $list->from,
                'to' => $list->to,
                'rangePageMax' => 3
            ],
            'documents' => $list->data
        ];

        return response()->json([
            'status' => 200,
            'message' => 'Lấy dánh ách tài liệu chung thành công',
            'data' => $response
        ]);
    }

    public function listByUser(Request $request)
    {
        $query = Document::query();
        $data = $request->all();

        // if (isset($data['keyword'])) {
            $query->where(function ($q) use ($data) {
                $q->where('user_id', $data['userId'])->where('title', 'like', '%'.$data['keyword'].'%');
            });
        // } else {
            // dd('abc');
        // }
        $query->with('department');

        $list = $query->paginate(10);

        $list = json_encode($list);
        $list = json_decode($list);

        $response = [
            'pagination' => [
                'total' => $list->total,
                'perPage' => $list->per_page,
                'currentPage' => $list->current_page,
                'lastPage' => $list->last_page,
                'from' => $list->from,
                'to' => $list->to,
                'rangePageMax' => 3
            ],
            'documents' => $list->data
        ];

        return response()->json([
            'status' => 200,
            'message' => 'Lấy danh sách tài liệu của người dùng cá biệt thành công',
            'data' => $response
        ]);        
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data = array_merge($data, [
            'user_id' => auth()->user()->id
        ]);
        
        $validator = $this->validator($data);

        if (!$validator->fails()) {
            Document::create($data);
            $document = Document::orderBy('id', 'desc')->first();

            return response()->json([
                'status' => 200,
                'message' => 'Thêm mới tài liệu thành công',
                'data' => $document
            ]);
        } else {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors()->messages()
            ]);
        }
    }

    public function show($id)
    {
        $document = Document::findOrFail($id);
        
        return response()->json([
            'status' => 200,
            'message' => 'Thành công',
            'data' => $document,
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $validator = $this->validator($data);

        if (!$validator->fails()) {
            $document = Document::findOrFail($id);
            $data = array_merge($data, [
                'user_id' => auth()->user()->id
            ]);
            $document->update($data);

            return response()->json([
                'status' => 200,
                'message' => 'Cập nhật tài liệu thành công',
                'data' => Document::find($id),
            ]);
        } else {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors()->messages()
            ]);
        }
    }

    public function delete(Request $request, $id)
    {
        $data = $request->all();
        
        $validator = $this->validator($data);

        // if (!$validator->fails()) {
            $document = Document::findOrFail($id);
            if($document->delete()){
                $query = Document::query();

                $query->where(function ($q) use ($data) {
                    $q->where('user_id', $data['userId']);
                });
                $query->with('department');

                $list = $query->paginate(10);

                $list = json_encode($list);
                $list = json_decode($list);

                $response = [
                    'pagination' => [
                        'total' => $list->total,
                        'perPage' => $list->per_page,
                        'currentPage' => $list->current_page,
                        'lastPage' => $list->last_page,
                        'from' => $list->from,
                        'to' => $list->to,
                        'rangePageMax' => 3
                    ],
                    'documents' => $list->data
                ];

                return response()->json([
                    'status' => 200,
                    'message' => 'Xóa tài liệu thành công',
                    'data' => $response
                ]);
        }
    }
    public function adminDelete(Request $request, $id)
    {
        $data = $request->all();
        
        // $validator = $this->validator($data);

        // if (!$validator->fails()) {
            $document = Document::findOrFail($id);
            if($document->delete()){
                $query = Document::query();

                // if (isset($data['keyword'])) {
                //     $query->where(function ($q) use ($data) { 
                //         $q->where('title', 'like', '%'.$data['keyword'].'%');
                //     });
                // }

                $query = $query->with('department');

                $list = $query->paginate(100);

                $list = json_encode($list);
                $list = json_decode($list);

                $response = [
                    'pagination' => [
                        'total' => $list->total,
                        'perPage' => $list->per_page,
                        'currentPage' => $list->current_page,
                        'lastPage' => $list->last_page,
                        'from' => $list->from,
                        'to' => $list->to,
                        'rangePageMax' => 3
                    ],
                    'documents' => $list->data
                ];

                return response()->json([
                    'status' => 200,
                    'message' => 'Lấy dánh sách tài liệu chung thành công',
                    'data' => $response
                ]);
        }
    }

    public function validator(array $data)
    {
        $attributes = [];

        $rules = [
            'title' => 'required'
        ];

        $messages = [
            'title.required' => 'Tiêu đề không được để trống'
        ];

        return Validator::make($data, $rules, $messages, $attributes);
    }

    public function getDocumentOfMe()
    {
        $auth_id = auth()->user()->id;
        $query = Document::where('user_id', $auth_id);
        $list = $query->paginate(10);

        $list = json_encode($list);
        $list = json_decode($list);

        $response = [
            'pagination' => [
                'total' => $list->total,
                'perPage' => $list->per_page,
                'currentPage' => $list->current_page,
                'lastPage' => $list->last_page,
                'from' => $list->from,
                'to' => $list->to,
                'rangePageMax' => 3
            ],
            'documents' => $list->data
        ];

        return response()->json([
            'status' => 200,
            'message' => 'Lấy tài liệu cá nhân thành công',
            'data' => $response
        ]);
    }
}
