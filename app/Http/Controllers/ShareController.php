<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Share;
use App\Models\User;
use Illuminate\Http\Request;

class ShareController extends Controller
{
    public function list(Request $request)
    {
        // $query = Share::query();
        $data = $request->all();
        // $receive = $data['receive_id'];
        $receiver = $data['receiveId'];
        // dd($receiver);
        // lấy document id trong model share theo receive id gửi lên
        $documentIds = Share::where('receive_id', $receiver)->get()->pluck('document_id')->toArray();
        $query = Document::whereIn('id', $documentIds);

        $query = $query->with('department', 'user');

        $publicIds = Document::where('public', 1)->get()->pluck('id')->toArray();
        // $publicDocs = Document::where('public', 1)->get()->keyBy('id');
        // $publicDocs->get($receiver);
        $publicDocs = Document::whereIn('id', $publicIds);

        $publicDocs = $publicDocs->with('department', 'user');


        $list = $query->paginate(100);

        $list = json_encode($list);
        $list = json_decode($list);
        //
        $list2 = $publicDocs->paginate(100);

        $list2 = json_encode($list2);
        $list2 = json_decode($list2);

        $response = [
            'pagination' => [
                'total' => $list->total,
                'perPage' => $list->per_page,
                'currentPage' => $list->current_page,
                'lastPage' => $list->last_page,
                'from' => $list->from,
                'to' => $list->to,
                'rangePageMax' => 3
            ],
            'shares' => $list->data,
            'publicDocs' => $list2->data
        ];

        return response()->json([
            'status' => 200,
            'message' => 'Lấy list share thành công',
            'data' => $response
        ]);
    }

    public function share(Request $request) {
        $data = $request->all();
        $from = $data['from_id'];
        $receive = $data['receive_id'];
        $document = $data['document_id'];

        Share::where([
            'from_id' => $from,
            'receive_id' => $receive,
            'document_id' => $document
        ])->delete();

        Share::create([
            'from_id' => $from,
            'receive_id' => $receive,
            'status' => $data['status'],
            'document_id' => $data['document_id']
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Thêm mới share thành công'
        ]);
    }

    public function show(Request $request)
    {
        // $document = Share::findOrFail($id);
        $data = $request->all();
        // $receive = $data['receive_id'];
        $receiver = $data['receiveId'];
        $docId = $data['documentId'];
        // lấy document id trong model share theo receive id gửi lên
        $documentIds = Share::where([
            'receive_id'=> $receiver,
            'document_id' => $docId
        ])->get()->pluck('from_id')->toArray();
        // $documentIds = Share::where('receive_id', $receiver)
        $query = User::whereIn('id', $documentIds)->get();

        // $list = $query->paginate(1000);

        // $list = json_encode($list);
        // $list = json_decode($list);

        // $response = [
        //     'pagination' => [
        //         'total' => $list->total,
        //         'perPage' => $list->per_page,
        //         'currentPage' => $list->current_page,
        //         'lastPage' => $list->last_page,
        //         'from' => $list->from,
        //         'to' => $list->to,
        //         'rangePageMax' => 3
        //     ],
        //     'shares' => $list->data
        // ];

        return response()->json([
            'status' => 200,
            'message' => 'Lấy thông tin người share tài liệu thành công',
            'data' => $query
        ]);
    }
}
