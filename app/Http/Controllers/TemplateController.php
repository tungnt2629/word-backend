<?php

namespace App\Http\Controllers;

use App\Events\DocumentCreatedEvent;
use Illuminate\Http\Request;
use App\Models\Template;
use App\Models\DepartmentTemplate;
use Illuminate\Support\Facades\Validator;

class TemplateController extends Controller
{
    public function list(Request $request)
    {
        $query = Template::query();
        $data = $request->all();

        if (isset($data['keyword'])) {
            $query->where(function ($q) use ($data) { 
                $q->where('title', 'like', '%'.$data['keyword'].'%');
            });
        }

        $list = $query->paginate(1000);

        $list = json_encode($list);
        $list = json_decode($list);

        $response = [
            'pagination' => [
                'total' => $list->total,
                'perPage' => $list->per_page,
                'currentPage' => $list->current_page,
                'lastPage' => $list->last_page,
                'from' => $list->from,
                'to' => $list->to,
                'rangePageMax' => 3
            ],
            'documents' => $list->data
        ];

        return response()->json([
            'status' => 200,
            'message' => 'Lấy list template thành công',
            'data' => $response
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        
        $validator = $this->validator($data);

        if (!$validator->fails()) {
            $created = Template::create($data);
           
            if ($created) {
                DepartmentTemplate::create([
                    'department_id' => $data['department_id'],
                    'template_id' => $created->id
                ]);
                $template = Template::orderBy('id', 'desc')->first();

                return response()->json([
                    'status' => 200,
                    'message' => 'Thêm mới tài liệu thành công',
                    'data' => $template
                ]);
            }
        } else {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors()->messages()
            ]);
        }
    }

    public function show($id)
    {
        $template = Template::findOrFail($id);
        
        return response()->json([
            'status' => 200,
            'message' => 'Thành công',
            'data' => $template,
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $validator = $this->validator($data);

        if (!$validator->fails()) {
            $template = Template::findOrFail($id);
            $template->update($data);

            DepartmentTemplate::where('template_id', $id)->delete();
            DepartmentTemplate::create([
                'department_id' => $data['department_id'],
                'template_id' => $id
            ]);

            return response()->json([
                'status' => 200,
                'message' => 'Cập nhật tài liệu thành công',
                'data' => Template::find($id),
            ]);
        } else {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors()->messages()
            ]);
        }
    }

    public function delete($id)
    {
        $template = Template::findOrFail($id);
        if($template->delete()){
            return response()->json([
                'status' => 200,
                'message' => 'Xóa tài liệu thành công',
            ]);
        }
    }

    public function validator(array $data)
    {
        $attributes = [];

        $rules = [
            'title' => 'required'
        ];

        $messages = [
            'title.required' => 'Tiêu đề không được để trống'
        ];

        return Validator::make($data, $rules, $messages, $attributes);
    }
}
