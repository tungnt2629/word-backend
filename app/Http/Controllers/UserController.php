<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    public function list(Request $request)
    {
        $query = User::query();
        $data = $request->all();

        if (isset($data['keyword'])) {
            $query->where(function ($q) use ($data) { 
                $q->where('title', 'like', '%'.$data['keyword'].'%');
            });
        }
        
        $list = $query->paginate(10);

        $list = json_encode($list);
        $list = json_decode($list);

        $response = [
            'pagination' => [
                'total' => $list->total,
                'perPage' => $list->per_page,
                'currentPage' => $list->current_page,
                'lastPage' => $list->last_page,
                'from' => $list->from,
                'to' => $list->to,
                'rangePageMax' => 3
            ],
            'users' => $list->data
        ];

        return response()->json([
            'status' => 200,
            'message' => 'Lấy danh sách người dùng thành công',
            'data' => $response
        ]);
    }

    public function listUserAdmin(Request $request)
    {
        $query = User::query()->with(['department' => function ($query) {
            $query->select('id', 'name');
        }]);
        $data = $request->all();

        if (isset($data['keyword'])) {
            $query->where(function ($q) use ($data) { 
                $q->where('name', 'like', '%'.$data['keyword'].'%');
            });
        }
        
        $list = $query->paginate(100);

        $list = json_encode($list);
        $list = json_decode($list);

        $response = [
            'pagination' => [
                'total' => $list->total,
                'perPage' => $list->per_page,
                'currentPage' => $list->current_page,
                'lastPage' => $list->last_page,
                'from' => $list->from,
                'to' => $list->to,
                'rangePageMax' => 3
            ],
            'users' => $list->data
        ];

        return response()->json([
            'status' => 200,
            'message' => 'Lấy danh sách người dùng thành công',
            'data' => $response
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
// dd($data);
        if (isset($data['password'])) {
            $data = array_merge($data, [
                'password' => bcrypt($data['password'])
            ]);
        }

        if (isset($data['avatar'])) {
            $folder = 'uploads/users/avatar/';
            if (!File::exists($folder)) {
                File::makeDirectory($folder, 0777, true);
            }
            $fileName = time().'.' . explode('/', explode(':', substr($data['avatar'], 0, strpos($data['avatar'], ';')))[1])[1];
            $imagePath = $folder.$fileName;
            \Image::make($data['avatar'])->save(public_path($folder).$fileName);

            $data = array_merge($data, [
                'avatar' => '/'.$imagePath
            ]);
        }
        
        $validator = $this->validator($data);

        
        if (!$validator->fails()) {
            User::create($data);
            $user = User::orderBy('id', 'desc')->first();

            return response()->json([
                'status' => 200,
                'message' => 'Thêm mới người dùng thành công',
                'data' => $user
            ]);
        } else {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors()->messages()
            ]);
        }
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        
        return response()->json([
            'status' => 200,
            'message' => 'Thành công',
            'data' => $user,
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $validator = $this->validator($data);

        if (!$validator->fails()) {
            $user = User::findOrFail($id);
            if (isset($data['password'])) {
                $data = array_merge($data, [
                    'password' => bcrypt($data['password'])
                ]);
            }

            $user = User::find($id);
            if (isset($data['avatar']) && $user->avatar != $data['avatar']) {
                $folder = 'uploads/users/avatar/';
                if (!File::exists($folder)) {
                    File::makeDirectory($folder, 0777, true);
                }
                $fileName = time().'.' . explode('/', explode(':', substr($data['avatar'], 0, strpos($data['avatar'], ';')))[1])[1];
                $imagePath = $folder.$fileName;
                \Image::make($data['avatar'])->save(public_path($folder).$fileName);
    
                $data = array_merge($data, [
                    'avatar' => '/'.$imagePath
                ]);
            }

            $user->update($data);

            return response()->json([
                'status' => 200,
                'message' => 'Cập nhật người dùng thành công',
                'data' => User::find($id),
            ]);
        } else {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors()->messages()
            ]);
        }
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return response()->json([
            'status' => 200,
            'message' => 'Xóa người dùng thành công'
        ]);
    }

    public function validator(array $data)
    {
        $attributes = [];

        $rules = [
            'email' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'gender' => 'required',
            'dob' => 'required',
        ];

        $messages = [
            'email.required' => 'Email không được để trống',
            'name.required' => 'Tên không được để trống',
            'phone.required' => 'Số điện thoại không được để trống',
            'gender.required' => 'Giới tính không được để trống',
            'dob.required' => 'Ngày sinh không được để trống',
        ];

        return Validator::make($data, $rules, $messages, $attributes);
    }

    public function getDocumentOfMe()
    {
        $auth_id = auth()->user()->id;
        $query = User::where('user_id', $auth_id);
        $list = $query->paginate(10);

        $list = json_encode($list);
        $list = json_decode($list);

        $response = [
            'pagination' => [
                'total' => $list->total,
                'perPage' => $list->per_page,
                'currentPage' => $list->current_page,
                'lastPage' => $list->last_page,
                'from' => $list->from,
                'to' => $list->to,
                'rangePageMax' => 3
            ],
            'users' => $list->data
        ];

        return response()->json([
            'status' => 200,
            'message' => 'lấy tài liệu cá nhân thành công',
            'data' => $response
        ]);
    }
}
