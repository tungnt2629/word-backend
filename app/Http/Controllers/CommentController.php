<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Document;
use App\Models\Share;
use App\Models\User;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function list(Request $request)
    {
        // $query = Share::query();
        $data = $request->all();
        $documentId = $data['docId'];

        // lấy user id trong model share theo receive id gửi lên
        $userIds = Comment::where('document_id', $documentId)->get()->pluck('user_id')->toArray();
        // dd($userIds);
        $userDetails = User::whereIn('id', $userIds)->get()->toArray();
        // dd($userDetails);
        $commentLists = Comment::where('document_id', $documentId);

        $list = $commentLists->paginate(100);

        $list = json_encode($list);
        $list = json_decode($list);
        
        $response = [
            'pagination_comment_list' => [
                'total' => $list->total,
                'perPage' => $list->per_page,
                'currentPage' => $list->current_page,
                'lastPage' => $list->last_page,
                'from' => $list->from,
                'to' => $list->to,
                'rangePageMax' => 3
            ],
            'listComments' => [
                'list' => $list->data,
                'userDetails' => $userDetails,
            ],
            
        ];

        return response()->json([
            'status' => 200,
            'message' => 'Lấy list comment thành công',
            'data' => $response
        ]);
    }

    public function comment(Request $request) {
        $data = $request->all();
        $from = $data['user_id'];
        $document = $data['document_id'];
        $content = $data['content'];
        $index = $data['index'];
        $length = $data['length'];

        // trùng -> xóa
        Comment::where([
            'user_id' => $from,
            'document_id' => $document,
            'content' => $content,
            'index' => $index,
            'length' => $length
        ])->delete();
        
        $newComment = Comment::create([
            'user_id' => $from,
            'document_id' => $document,
            'content' => $content,
            'index' => $index,
            'length' => $length
        ]);

        $commentLists = Comment::where('document_id', $document);
        $list = $commentLists->paginate(100);

        $list = json_encode($list);
        $list = json_decode($list);

        $response = [
            'pagination_comment_list' => [
                'total' => $list->total,
                'perPage' => $list->per_page,
                'currentPage' => $list->current_page,
                'lastPage' => $list->last_page,
                'from' => $list->from,
                'to' => $list->to,
                'rangePageMax' => 3
            ],
            'listComments' => $list->data,
            'newComment' => $newComment
        ];
        // $newComment = Comment::where('')
        return response()->json([
            'code' => 200,
            'message' => 'Thêm mới comment thành công',
            'data' => $response
        ]);
    }

    public function show($id)
    {
        // $document = Share::findOrFail($id);
        
        $receiver = $id;
        // lấy document id trong model share theo receive id gửi lên
        $documentIds = Share::where('receive_id', $receiver)->get()->pluck('from_id')->toArray();
        $query = User::where('id', $documentIds)->get();

        // $list = $query->paginate(1000);

        // $list = json_encode($list);
        // $list = json_decode($list);

        // $response = [
        //     'pagination' => [
        //         'total' => $list->total,
        //         'perPage' => $list->per_page,
        //         'currentPage' => $list->current_page,
        //         'lastPage' => $list->last_page,
        //         'from' => $list->from,
        //         'to' => $list->to,
        //         'rangePageMax' => 3
        //     ],
        //     'shares' => $list->data
        // ];

        return response()->json([
            'status' => 200,
            'message' => 'Lấy thông tin người share tài liệu thành công',
            'data' => $query
        ]);
    }

    public function delete(Request $request)
    {
        $data = $request->all();
        $userId = $data['userId'];
        $docId = $data['docId'];
        $content = $data['content'];
        // dd($docId);
        Comment::where([
            'user_id' => $userId,
            'document_id' => $docId,
            'content' => $content,
        ])->delete();

        // lấy user id trong model share theo receive id gửi lên
        $userIds = Comment::where('document_id', $docId)->get()->pluck('user_id')->toArray();
        // dd($userIds);
        $userDetails = User::whereIn('id', $userIds)->get()->toArray();
        // dd($userDetails);
        $commentLists = Comment::where('document_id', $docId);

        $list = $commentLists->paginate(100);

        $list = json_encode($list);
        $list = json_decode($list);
        
        $response = [
            'pagination_comment_list' => [
                'total' => $list->total,
                'perPage' => $list->per_page,
                'currentPage' => $list->current_page,
                'lastPage' => $list->last_page,
                'from' => $list->from,
                'to' => $list->to,
                'rangePageMax' => 3
            ],
            'listComments' => [
                'list' => $list->data,
                'userDetails' => $userDetails,
            ],
            
        ];

        return response()->json([
            'status' => 200,
            'message' => 'Xóa comment và Lấy list comment thành công',
            'data' => $response
        ]);
    }
}
