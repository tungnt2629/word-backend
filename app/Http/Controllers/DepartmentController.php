<?php

namespace App\Http\Controllers;

use App\Events\DocumentCreatedEvent;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{
    public function list(Request $request)
    {
        $query = Department::query();
        $query->with('users');
        $data = $request->all();

        if (isset($data['keyword'])) {
            $query->where(function ($q) use ($data) { 
                $q->where('title', 'like', '%'.$data['keyword'].'%');
            });
        }
        $query->with('templates');

        $list = $query->paginate(100);

        $list = json_encode($list);
        $list = json_decode($list);

        $response = [
            'pagination' => [
                'total' => $list->total,
                'perPage' => $list->per_page,
                'currentPage' => $list->current_page,
                'lastPage' => $list->last_page,
                'from' => $list->from,
                'to' => $list->to,
                'rangePageMax' => 3
            ],
            'departments' => $list->data
        ];

        return response()->json([
            'status' => 200,
            'message' => 'Lấy danh sách phòng ban thành công',
            'data' => $response
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        
        $validator = $this->validator($data);

        if (!$validator->fails()) {
            Department::create($data);
            $department = Department::orderBy('id', 'desc')->first();

            return response()->json([
                'status' => 200,
                'message' => 'Thêm mới phòng ban thành công',
                'data' => $department
            ]);
        } else {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors()->messages()
            ]);
        }
    }

    public function show($id)
    {
        $department = Department::with('templates')->where('id', $id)->first();
        
        return response()->json([
            'status' => 200,
            'message' => 'Thành công',
            'data' => $department,
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $validator = $this->validator($data);

        if (!$validator->fails()) {
            $department = Department::findOrFail($id);
            $department->update($data);

            return response()->json([
                'status' => 200,
                'message' => 'Cập nhật phòng ban thành công',
                'data' => Department::find($id),
            ]);
        } else {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors()->messages()
            ]);
        }
    }

    public function delete(Request $request, $id)
    {
        $data = $request->all();
        
        $validator = $this->validator($data);

        // if (!$validator->fails()) {
            $department = Department::findOrFail($id);
            if($department->delete()){
                return response()->json([
                    'status' => 200,
                    'message' => 'Xóa phòng ban thành công',
                    // 'data' => Document::find($id),
                ]);
            } else {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors()->messages()
            ]);
        }
    }

    public function validator(array $data)
    {
        $attributes = [];

        $rules = [
            'name' => 'required'
        ];

        $messages = [
            'name.required' => 'Tên phòng ban không được để trống'
        ];

        return Validator::make($data, $rules, $messages, $attributes);
    }
}
