<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->all();

        $attribute = [
            'name' => __('name'),
            'phone' => __('phone'),
            'gender' => __('gender'),
            'dob' => __('dob'),
            'avatar' => __('avatar'),
            'email' => __('email'),
            'password' => __('password'),
            'department_id' => __('department_id'),
        ];

        $rules = [
            'name'     => 'required',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:8',
        ];

        $messages = [
            'name.required' => 'Tên không được để trống',
            'email.required' => 'Email không được để trống',
            'email.unique' => 'Email đã tồn tại, vui lòng nhập địa chỉ email khác',
            'password.required' => 'Mật khẩu không được để trống',
            'department_id.required' => 'Vui lòng chọn phòng ban',
        ];

        $validator = Validator::make($request->all(), $rules, $messages, $attribute);

        if (!$validator->fails()) {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'gender' => $data['gender'],
                'dob' => $data['dob'],
                'avatar' => $data['avatar'],
                'password' => Hash::make($data['password']),
                'department_id' => $data['department_id']
            ]);
    
            $token = $user->createToken('auth_token')->plainTextToken;
    
            return response()->json([
                'status' => 200,
                'access_token' => $token,
                'token_type' => 'Bearer',
            ]);
        } else {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors()->messages()
            ]);
        }
    }

    public function login(Request $request)
    {
        $attribute = [
            'email' => __('email'),
            'password' => __('password'),
        ];

        $rules = [
            'email'     => 'required|string|email|max:255',
            'password'  => 'required|string|min:8',
        ];

        $messages = [
            'email.required' => 'Email không được để trống',
            'password.required' => 'Mật khẩu không được để trống',
        ];

        $validator = Validator::make($request->all(), $rules, $messages, $attribute);

        if (!$validator->fails()) {
            if (!Auth::attempt($request->only('email', 'password'))) {
                return response()->json([
                    'status' => 401,
                    'message' => 'Sai tài khoản hoặc mật khẩu!'
                ]);
            }
    
            $user = User::where('email', $request['email'])->firstOrFail();
    
            $token = $user->createToken('auth_token')->plainTextToken;
    
            return response()->json([
                'status' => 200,
                'access_token' => $token,
                // 'role'-> $user
                'token_type' => 'Bearer',
            ]);
        } else {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors()->messages()
            ]);
        }
    }

    public function me(Request $request)
    {
        return response()->json([
            'status' => 200,
            'data' => $request->user()
        ]);
    }

    public function updateInfo(UserUpdateRequest $request)
    {
        $data = $request->all();
        $authId = auth()->user()->id;
        $user = User::find($authId);

        if (isset($data['avatar']) && $user->avatar != $data['avatar']) {
            $folder = 'uploads/users/avatar/';
            if (!File::exists($folder)) {
                File::makeDirectory($folder, 0777, true);
            }
            $fileName = time().'.' . explode('/', explode(':', substr($data['avatar'], 0, strpos($data['avatar'], ';')))[1])[1];
            $imagePath = $folder.$fileName;
            \Image::make($data['avatar'])->save(public_path($folder).$fileName);

            $data = array_merge($data, [
                'avatar' => '/'.$imagePath
            ]);
        }

        $user->update($data);

        return response()->json([
            'status' => 200,
            'message' => 'Cập nhật thông tin tài khoản thành công',
            'data' => auth()->user()
        ]);
    }

    public function validator(array $data)
    {
        $attribute = [
            'name' => __('name'),
            'email' => __('email'),
            'password' => __('password'),
        ];

        $rules = [
            'name'     => 'required',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:8',
        ];

        $messages = [
            'name.required' => 'Tên không được để trống',
            'email.required' => 'Email không được để trống',
            'email.unique' => 'Email đã tồn tại, vui lòng nhập địa chỉ email khác',
            'password.required' => 'Mật khẩu không được để trống',
        ];

        return Validator::make($data, $rules, $messages, $attribute);
    }

    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => ['required', 'string', 'max:255'],
    //         'dob' => ['required'],
    //         'gender' => ['required'],
    //         'phone' => ['required', 'string', 'max:15', 'min:8'],
    //         'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    //         'password' => ['required', 'string', 'min:8', 'confirmed'],
    //         ]);
    // }
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'phone' => $data['phone'],
            'gender' => $data['gender'],
            'dob' => $data['dob'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
    ]);
    }
}
