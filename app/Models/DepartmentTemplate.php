<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentTemplate extends Model
{
    use HasFactory;

    protected $table = 'department_templates';

    protected $fillable = [
        'department_id',
        'template_id'
    ];
}
