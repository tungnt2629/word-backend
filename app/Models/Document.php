<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    protected $table = 'documents';

    protected $fillable = [
        'title',
        'content',
        'user_id',
        'department_id',
        'comment',
        'public',
    ];

    public function comments() {
        return $this->belongsTo('App\Models\Comment', 'document_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    
    public function department() {
        return $this->belongsTo('App\Models\Department', 'department_id', 'id');
    }
}
