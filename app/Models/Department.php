<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';
    
    protected $fillable = [
        'name',
        // 'usercount'
    ];
    public function templates() {
        return $this->belongsToMany('App\Models\Template', 'department_templates', 'department_id', 'template_id');
    }

    public function users(){
		return $this->hasMany('App\Models\User', 'department_id', 'id');
	}
}
