<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use HasFactory;

    protected $table = 'templates';

    protected $fillable = [
        'title',
        'content'
    ];

    public function departments() {
        return $this->belongsToMany('App\Models\Department', 'department_templates', 'department_id', 'template_id');
    }
}
