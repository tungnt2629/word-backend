<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\ShareController;
use App\Http\Controllers\TemplateController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::get('/departments', [DepartmentController::class, 'list']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('/me', [AuthController::class, 'me']);
    Route::post('/user/update-info', [AuthController::class, 'updateInfo']);

    Route::group(['prefix' => 'documents'], function () {
        Route::get('/', [DocumentController::class, 'list']);
        Route::post('/', [DocumentController::class, 'listByUser']);
        Route::post('/create', [DocumentController::class, 'store']);
        Route::get('/show/{id}', [DocumentController::class, 'show']);
        Route::post('/update/{id}', [DocumentController::class, 'update']);
        Route::post('/delete/{id}', [DocumentController::class, 'delete']);
        Route::post('/admin/delete/{id}', [DocumentController::class, 'adminDelete']);
        Route::get('/me', [Documentcontroller::class, 'getDocumentOfMe']);
    });

    Route::group(['prefix' => 'share'], function () {
        Route::post('/', [ShareController::class, 'share']);
        Route::post('/list', [ShareController::class, 'list']);
        Route::post('/show', [ShareController::class, 'show']);
    });

    Route::group(['prefix' => 'comments'], function () {
        Route::post('/', [CommentController::class, 'comment']);
        Route::post('/list', [CommentController::class, 'list']);
        Route::get('/show/{id}', [CommentController::class, 'show']);
        Route::post('/delete', [CommentController::class, 'delete']);
    });

    Route::group(['prefix' => 'departments'], function () {
        Route::post('/create', [DepartmentController::class, 'store']);
        Route::get('/show/{id}', [DepartmentController::class, 'show']);
        Route::post('/update/{id}', [DepartmentController::class, 'update']);
        Route::post('/delete/{id}', [DepartmentController::class, 'delete']);
    });

    Route::group(['prefix' => 'templates'], function () {
        Route::get('/', [TemplateController::class, 'list']);
        Route::post('/create', [TemplateController::class, 'store']);
        Route::get('/show/{id}', [TemplateController::class, 'show']);
        Route::post('/update/{id}', [TemplateController::class, 'update']);
        Route::post('/delete/{id}', [TemplateController::class, 'delete']);
    });
   
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UserController::class, 'list']);
        Route::get('/listUserAdmin', [UserController::class, 'listUserAdmin']);
        Route::post('/create', [UserController::class, 'store']);
        Route::get('/show/{id}', [UserController::class, 'show']);
        Route::post('/update/{id}', [UserController::class, 'update']);
        Route::post('/delete/{id}', [UserController::class, 'delete']);
    });
});