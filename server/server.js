// import axios from 'axios'

// const mongoose = require("mongoose")
// const Document = require("./Document")
// const { ObjectId } = require('mongodb');

// mongoose.connect("mongodb://localhost/google-docs-clone", {
//   useNewUrlParser: true,
//   useUnifiedTopology: true,
//   useFindAndModify: false,
//   useCreateIndex: true,
// }) 

const io = require("socket.io")(3001, {
  cors: {
    // origin: "https://main.ds40qjaqr71er.amplifyapp.com",
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
  },
})
// io.set('transports', ['websocket']);

const defaultValue = ""

// get currentdocument by id
// const document

io.on("connection", socket => {
  socket.on("get-document", async datas => {
    console.log("user a get from client", datas.usera)
    
    // const document = await findOrCreateDocument(datas.id, datas.content)
    // console.log("server response HELLO", document.data)
    socket.join(datas.id)
    // socket.emit("load-document", document.data)
    socket.emit("load-document", datas.content)

    socket.emit("load-usera", datas.usera)

    socket.on("send-changes", delta => {
      socket.broadcast.to(datas.id).emit("receive-changes", delta)
    })
    socket.on("send-users", data => {
      socket.broadcast.to(datas.id).emit("receive-users", data)
    })

    // socket.on("save-document", async data => {
    //   // console.log("emit save doc", data)
    //   await Document.findByIdAndUpdate(datas.id, { data })
    // })

    // socket.on("save-document-laravel", async (data, token) => {
    //   // await console.log("-----> SERVER HELLO", data, token)
    //   // api call to save data to sql

    //   // const document = await findOrCreateDocument(documentId)
    //   // console.log(document.data)
    // })
  })
})

// async function findOrCreateDocument(id, apiData) {
//   if (id == null) return

//   const document = await Document.findById(id)
//   // console.log("ELSE", apiData)
//   if (document) {
//     // console.log("found doc in mongo", document.data)
//     const loadedDoc = await Document.findByIdAndUpdate(id, { apiData })
//     // const abc = await Document.findById(id)
//     // console.log(loadedDoc.data)
//     return loadedDoc
//     // return document
//   }
//   else return await Document.create({ _id: id, data: apiData })
// }
